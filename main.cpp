#include <iostream>

class Base {
private:
    int p = 1;

protected:
    int x = 2;

public:
    void test() {
        std::cout << p + x << std::endl;
    }

};

class Derived : public Base {

public:
    void NewTest() {
        x = 7;
        test();
    }
};

int main() {
    Base b;
    b.test();

    Derived d;

    d.NewTest();

    return 0;
}
